Application Name: food-recipe-api

Api Name: food-recipe-api

Description : This Api is Proxy for the below API (Search and Get).
API: http://food2fork.com/about/api - To use this Api we need to create a account and once registered they will provide a API key which needs to be used as a query parameter when sending the request to this API


Currently "food-recipe-api" supports two operations - Get Recipe details , Search Recipes

Get Recipe details
---------------------------------
HTTP Method : GET

queryParameters - 
		  rId: 
          description : Id of desired recipe as returned by Search Query
          type: any
          required : true

Proxy EndPoint Api - http://food-recipe-api.us-e2.cloudhub.io/v1/recipe/recipeDetails?rId=56f78c
                     http://food-recipe-api.us-e2.cloudhub.io/v1/recipe/recipeDetails?rId=1491ac 

Response from the API - 
        - When you trigger this API with recipeId(rId in the url), if the get recipe returns a recipe which contains 'egg' as an ingredient, it will throw error "Recipe contains egg!" otherwise it will return the recipe details.
		
		
How to trigger a get recipe details request through postman
		Url - http://food-recipe-api.us-e2.cloudhub.io/v1/recipe/recipeDetails?rId=56f78c
		Click Authorization Tab - Select Basic Auth from Drop down
		Provide Client_Id and Client_Secret in Username and Password fields respectively
		Click on Header Tab - Give Key as Content-Type and Value as application/json
		Click Send 
		Response will be available in Body Tab
		
		
Search Recipes
----------------------------------
HTTP Method : GET

queryParameters: 
        q: 
          description: Search Query (Ingredients should be separated by commas). If this is omitted top rated recipes will be returned.
          type: string
          required: true
        page:
          description: Used to get additional results 
          type: integer
          required: false

Proxy EndPoint Api - http://food-recipe-api.us-e2.cloudhub.io/v1/recipe/searchRecipe?q=all&page=2
                     http://food-recipe-api.us-e2.cloudhub.io/v1/recipe/searchRecipe?q=all&page=1
                     http://food-recipe-api.us-e2.cloudhub.io/v1/recipe/searchRecipe?q=Egg 

Response from the API - 
        - When you trigger this API with query search (q) and page which is number of set( each set return by default 30 Count- If you want to get more results trigger the same request by changing page value. Search recipe api returns response with egg recipes filtered means recipe with egg wont be fetched. If a user searches for a recipe which contains egg , it will throw through "This feature is not supported!".
		
		
How to trigger a get recipe details request through postman
		Url - http://food-recipe-api.us-e2.cloudhub.io/v1/recipe/searchRecipe?q=all&page=1
		Click Authorization Tab - Select Basic Auth from Drop down
		Provide Client_Id and Client_Secret in Username and Password fields respectively
		Click on Header Tab - Give Key as Content-Type and Value as application/json
		Click Send 
		Response will be available in Body Tab
		
All the Basic exceptions like 
                   Unauthorized access , 
				   Bad Request  
				   Invalid Input
				   MethodNotAllowed
				   ResourceNotFound
				   InternalError
				   ServiceUnavailable
are handled by the application

					 