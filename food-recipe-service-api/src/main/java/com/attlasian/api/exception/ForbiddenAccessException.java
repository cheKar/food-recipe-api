package com.attlasian.api.exception;

import org.mule.module.apikit.exception.MuleRestException;

public class ForbiddenAccessException extends MuleRestException{

	private static final long serialVersionUID = 1L;
	
	public ForbiddenAccessException(String message){
		super(message);
	}

}
