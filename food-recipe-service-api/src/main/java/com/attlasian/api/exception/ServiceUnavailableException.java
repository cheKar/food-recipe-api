package com.attlasian.api.exception;

import org.mule.module.apikit.exception.MuleRestException;

public class ServiceUnavailableException extends MuleRestException{

	private static final long serialVersionUID = 1L;
	
	public ServiceUnavailableException(String message){
		super(message);
	}

}
